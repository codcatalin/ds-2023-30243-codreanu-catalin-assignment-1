package ro.tuc.ds2020.enums;

public enum UserRole {
    User,
    Admin
}
